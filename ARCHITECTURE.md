# Architecture

This document serves as an overview of the architecture, thoughts for future work (which are mostly in GitLab Issues), and houses historical developer thoughts and notes.

## Design

### Pieces

The following pieces will be needed:

- AWS Lambda (scheduled on a cron) to run the logic
  - AWS Lambda currently only supports up to Python 3.9 so we have to use that for the Python portions of the CI process.
- Small EFS instance for the database (sqlite)
- Slack Bot to handle messaging users

### User story

#### Signing up for the service

When Alice joined the #virtual-coffee channel, she was asked by the Virtual Coffee Slack Bot (VCSB) to introduce herself to it. The VCSB asked her some questions (e.g., favorite TV show, favorite movie, if she were a dragon what she would hoard) and then added that information to a database.

#### How matching happens

Alice and Bob are both in the #virtual-coffee channel on their Slack. On Monday at 9am EST, they get a group message that contains Alice, Bob, and the VCSB. The VCSB has messaged them to say, "You are paired for a virtual coffee this week. Please introduce yourselves and plan a time to meet."

The VCSB could even say, "Hi, Alice! Meet Bob. Here are some of the things he's interested in: \<thing 1>, \<thing 2>."

The VCSB sends individual messages to Alice and Bob and introduces the other, and then they can DM each other.

Alice and Bob can then chat and set up a time to meet that week.

### Constraints

- No matching twice within... three?... months

### Simplifying the installation process

- Wrap as much into Terraform as possible
- Set up Slack bot (make sure to document specific permissions needed)
- Can we go further and just have an install script? How do we handle adding the Slack Bot? That seems like a lot for each user.

### Database schema

#### user table

- user id
- DO NOT STORE NAMES
- one column for each interest (finite because we only ask so many questions)

#### matches table

- id 1
- id 2
- date

### Edge cases

- not enough people for a three month rotation, so need to match based on oldest match date
- 100 users, 1000 users?
