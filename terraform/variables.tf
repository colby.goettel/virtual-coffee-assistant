variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "The AWS region to use."
}

variable "SLACK_API_TOKEN" {
  type        = string
  description = "Your Slack app's API token. This should be defined as an environment variable called TF_VAR_SLACK_API_TOKEN either in your CLI or in GitLab."
}

variable "slack_channel_name" {
  type        = string
  description = "The channel name for your virtual coffee meetings (e.g., \"virtual-coffee\"). Do not include the hash symbol."
}

variable "vca_day_time" {
  type        = string
  default     = "0 14 ? * MON *"
  description = "The day and time you want matches to happen (must be in crontab format)."
}
