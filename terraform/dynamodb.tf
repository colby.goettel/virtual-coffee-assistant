# The Lambda function will run twice because there is no idempotency enforced. In order to check for duplicate runs, we need to create an idempotency table that the Lambda function can reference.
# Reference: https://awslabs.github.io/aws-lambda-powertools-python/2.4.0/utilities/idempotency/
module "dynamodb_table" {
  source = "terraform-aws-modules/dynamodb-table/aws"

  name     = "IdempotencyTable"
  hash_key = "id"

  ttl_attribute_name = "expiration"
  ttl_enabled        = true

  attributes = [
    {
      name = "id"
      type = "S"
    }
  ]
}
