# Lambda
output "lambda_function_arn" {
  value       = module.vca_lambda_function.lambda_function_arn
  description = "The ARN of the Lambda function."
}

# Function URL
output "lambda_function_url" {
  value       = module.vca_lambda_function.lambda_function_url
  description = "The Function URL for the Lambda function. This can be used to manually trigger the function. Appending `?key=value` to the URL will cause the function to run idempotently."
}
