provider "aws" {
  region = var.aws_region

  default_tags {
    tags = {
      purpose   = "Virtual coffee assistant Slack integration"
      Terraform = "true"
    }
  }
}

terraform {
  # GitLab will automatically configure the backend when the pipeline runs.
  backend "http" {}

  required_providers {
    aws = {
      version = ">= 4.24.0"
      source  = "hashicorp/aws"
    }
  }

  required_version = ">= 1.3.6"
}
