# Virtual coffee assistant

A Slack app that will automatically set up virtual coffees with people in a channel.

The Slack app is run by an AWS Lambda that takes all of the people in a channel, matches two of them together so they can have a virtual coffee, and messages each person to tell them about their match. The frequency is defined via cron with the default setting being once a week on Mondays at 9am EST.

## Installation

The very first thing you will want to do is fork this repository so that you can make the necessary changes and be in control of your deployment.

Before you can work through the following sections, you will need access to:

- An AWS account
- A Slack workspace and a channel that people can join to opt-in to this service. Throughout this document and the associated scripts, we will always call this channel "#virtual-coffee", although you can name it whatever you want.

The following sections will guide you through creating a Slack app, an AWS user and their permissions, and configuring Terraform for the installation. You can either deploy with a local installation of Terraform or through GitLab CI.

### Creating a Slack app

To create a new Slack app, you will need to:

- [Create a new app](https://api.slack.com/apps?new_app=1)
  - Select "From scratch"
    - App Name: "Virtual coffee assistant"
    - Under "Pick a workspace to develop your app in": Select the one you want
  - This will bring you to a new page for the Slack API. This is the place where administer your app so you may want to bookmark this page. You can also customize the App's avatar from here.
- From the app administration page:
  - Click on "OAth & Permissions" (in the left side pane).
  - Go down to Scopes and under "Bot Token Scopes" add the following permissions:
    - "chat:write"
    - "channels:read"
  - In the left pane, click on "Install App" and select "Install to Workspace" and then select "Allow" on the next page. You will now have the OAuth token for your app. You will need to put this in [`terraform/terraform.tfvars`](terraform/terraform.tfvars) under `slack_api_token`.

### AWS user and permissions

To deploy into AWS, you will need to create an AWS user:

1. Call your user "vca" and select that you want to use an access key for authentication. Click next.
1. For permissions, we will want to attach an existing policy, so select that option. Click on create policy. This will open a new tab where you can define your policy.
    1. Click over to the JSON tab
    1. Add the following JSON:

        ```json
                {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "VisualEditor0",
                    "Effect": "Allow",
                    "Action": [
                        "iam:*",
                        "logs:*",
                        "lambda:*",
                        "dynamodb:*",
                        "events:*"
                    ],
                    "Resource": "*"
                }
            ]
        }
        ```

    1. Click next.
    1. Add any tags if you'd like and click next.
    1. Name the policy "virtual-coffee-assistant-terraform" and click submit.
1. Go back to the user creation tab. Filter for the "virtual-coffee-assistant-terraform" policy and click the checkbox next to it. Click next.
1. Add any tags you'd like and click next.
1. Review and click "Create user".
1. It will then show you your access key ID and secret access key. You will need to add these to GitLab (instructions below) so make sure you hold onto them.

### Configuration

#### [`terraform/terraform.tfvars`](terraform/terraform.tfvars)

There are a few things you will need to add in [`terraform/terraform.tfvars`](terraform/terraform.tfvars):

- `aws_region`: The AWS region you want to deploy into. The default is "us-east-1".
- `slack_channel_name`: The Slack channel name. This is the name of the channel, not the channel ID. DO NOT include the hash symbol (e.g., use "virtual-coffee", NOT "#virtual-coffee").
- `vca_day_time`: The crontab entry for your virtual coffee assistant which controls how often meetings are scheduled. This needs to be input in crontab format. Note that [AWS Lambda syntax](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html) requires six fields. The default is every Monday at 9am EST, or, in crontab format: `0 14 ? * MON *`.

#### Environment variables

You will also need to set the following environment variables so that Terraform can run:

- `AWS_ACCESS_KEY_ID`: The AWS access key. Get this from running `aws configure`.
- `AWS_SECRET_ACCESS_KEY`: The AWS secret access key. Get this from running `aws configure`.
- `GITLAB_ACCESS_TOKEN`: (optional) You only need this to run the installation locally. To get this, create a personal access token with the API scope.
- `GITLAB_USERNAME`: (optional) You only need this to run the installation locally. Your GitLab username.
- `PROJECT_ID`: (optional) You only need this to run the installation locally. Your project's ID (at the top of the repo's page).
- `TF_RECREATE_MISSING_LAMBDA_PACKAGE`: Needs to be set to "false" so that previous deployment artifacts are used.
- `TF_VAR_SLACK_API_TOKEN`: Your Slack app's API token. For instructions, see [Creating a Slack app](#creating-a-slack-app).

You can set these locally if you're deploying locally by running something like:

```bash
export AWS_ACCESS_KEY_ID='id'
export AWS_SECRET_ACCESS_KEY='key'
export GITLAB_ACCESS_TOKEN='token'
export GITLAB_USERNAME='username'
export TF_RECREATE_MISSING_LAMBDA_PACKAGE='false'
export TF_VAR_SLACK_API_TOKEN='token'
```

Or, if you want to utilize the GitLab CI/CD pipeline to deploy, you will need to set them in GitLab:

1. In your repository, go to "Settings"
1. Click on "CI/CD"
1. You will add your environment variables in the "Variables" section. For each variable, click "Add variable" and do the following:
    1. Enter the name and value for your variable (from above)
    1. Unselect "Protect variable" unless you want the `main` branch to be protected (which you will need to do on your own)
    1. Select the "Mask variable" option

### Running Terraform to install the Lambda

Before you can install, make sure that you've run through all of the steps in the [Configuration](#configuration) section. If you are installing locally, you will need your environment variables set in the CLI. If you are installing through the GitLab CI process, make sure that you have defined your variables there.

#### Locally

The following commands will need to be run in the `terraform` directory:

    cd terraform

Once there, format your code:

    terraform fmt

Initialize Terraform:

```terraform
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/vca" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/vca/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/vca/lock" \
    -backend-config="username=$GITLAB_USERNAME" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

Validate that the Terraform code is correct:

    terraform validate

And deploy:

    terraform apply

#### Through GitLab CI

Make the changes mentioned in the [Installation](#installation) section, commit, and push. You can then view the progress of your deployment through the CI/CD menu (in the left pane). Click on the running pipeline to see the various stages and their progress.

You should be able to ignore the `validate` and `test` stages (but if something breaks there, please look into it and consider [filing an Issue](https://gitlab.com/colby.goettel/virtual-coffee-assistant/-/issues)). The main thing you'll want to look at is the `build` stage. If that succeeds, go to the `deploy` stage and manually trigger it. This will install the app.

### Testing that the Lambda works

Before telling your users about this new system, you and a few test subjects (who don't mind being spammed by the Lambda while you get up and running) should join your #virtual-coffee channel.

When Terraform runs (either locally or through the CI/CD pipeline), it will output a `lambda_function_url`. This Function URL is a public link that you can go to which will trigger a manual deployment of the Lambda function. Please note that this URL is public and doesn't require authorization, so don't go handing it out.

You can now use that link for manual deployments and then check with each other to see how matches are going. Or, if you'd prefer to use the logs, you will need to change the logging level to `INFO` and then you can view the events in AWS CloudWatch.

> 🛈 **Why is the function being called twice?**
>
> When the event is called normally, the event payload is the same so the function is only called once. However, when you call it with the Function URL, the function will be called twice. If you append `?key=value` to the URL, this shouldn't happen.

## Uninstalling the app

If you ever need to uninstall, you can either run `terraform destroy` if you deployed locally, or you can trigger the "uninstall" stage of the pipeline if you deployed through GitLab CI.

### Scorched earth

If you run into any issues with the pipeline and you can't tear down the deployment that way, you can do it on the command line, as follows:

1. Go to your repo on <https://gitlab.com>.
1. In the left pane, go to Infrastructure &rarr; Terraform.
1. Your state file will be listed (it should be called "vca"). Click the three dots under Actions and select "Copy Terraform init command"
1. A new window will pop up. Copy the init command it provides you.
1. On the command line, go to the repository and into the `terraform` directory. You will need a `GITLAB_ACCESS_TOKEN` for this ([see above](#environment-variables)).
1. Paste the init command and run it. This will initialize Terraform so it can talk to that state file.
1. Run `terraform destroy`. It will print out a bunch of information about what it's going to destroy and ask you if you want to do this. Type "yes" and hit enter.
