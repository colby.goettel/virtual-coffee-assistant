# Contributing

We would love help developing and maintaining this app.

## Getting started

Familiarize yourself with the codebase and take a look at the Issues tracker in GitLab. There are several portions to this app:

1. The AWS Lambda code. This is Python code and is in the `src/` directory (the app is [`app.py`](src/virtual_coffee_assistant/app.py) and there's also a requirements file for pip dependencies). The testing code is in the `tests/` directory.
    1. We are using docstrings for method documentation. Please maintain consistency with what is already there. Lots of examples to go off.
    1. A logger is configured and we would appreciate that if you add or edit existing functions, to please make sure that reasonable logging is happening. For example, it's nice to know which users are paired at the debugging level, the info level is good for a one-liner about what the method did, and so on.
    1. We're using [`black`](https://github.com/psf/black) for formatting.
1. The Terraform code. This is to deploy the app and the code is in the `terraform/` directory. The app can be deployed either manually or through GitLab CI.
    1. I have tried to document throughout with what is happening and why. Please add any documentation you think is necessary.
1. GitLab CI. The app can be deployed via GitLab CI. To get started with this code, check out [`.gitlab-ci.yml`](.gitlab-ci.yml) which houses the main part of the code and links to the other portions (e.g., Markdown, Python).
1. Documentation. The code is pretty well documented (please help keep it that way!). We also use the [README](README.md) and [ARCHITECTURE](ARCHITECTURE.md) files for documentation and planning.

Feel free to work on whichever part works best for you. If you would like help or pointers, please submit Issues or comment on existing ones.

If you add any code and you're wondering if there is enough documentation, my rule of thumb has always been: if you have to ask, you should probably just do it. I err on the side of too much documentation and I know that not everyone is that way, but here we are.

## How to

We are using the feature branch workflow for internal development. If you are not a contributor on the project, please fork and submit a merge request.
